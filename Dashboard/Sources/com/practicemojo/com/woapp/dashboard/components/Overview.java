package com.practicemojo.com.woapp.dashboard.components;

import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOContext;

public class Overview extends SHWOComponent {
	
    public Overview(WOContext context) {
        super(context);
    }
    
    @Override
	public String pageTitle() {
    	return "Overview: Widget Display";
    }

}