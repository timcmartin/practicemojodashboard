package com.practicemojo.com.woapp.dashboard.components;

import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOContext;

public class Dashboard extends SHWOComponent {
	
    public Dashboard(WOContext context) {
        super(context);
    }
    
    @Override
	public String pageTitle() {
    	return "Full Widget Display";
    }
}