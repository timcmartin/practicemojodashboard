package com.practicemojo.com.woapp.dashboard.widgets;

import java.text.SimpleDateFormat;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.practicemojo.businesslogic.customer.Customer;
import com.practicemojo.businesslogic.customer.CustomerTransaction;
import com.practicemojo.businesslogic.customer.CustomerTransactionDetail;
import com.practicemojo.businesslogic.customer.PatientAppointment;
import com.practicemojo.businesslogic.customer.PatientAppointmentConfirmation;
import com.practicemojo.businesslogic.customer.PatientPhone;
import com.practicemojo.businesslogic.reports.ReportUtilities;
import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXBatchFetchUtilities;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.foundation.ERXTimestampUtilities;

public class Appointments extends SHWOComponent {
	private static final Logger log = Logger.getLogger(Tester.class);
	public EOEditingContext editingContext = ERXEC.newEditingContext();
	// Testing Values
	private static int TEST_CUSTOMER = 2705;
	private static int START_DATE_INCREMENT = 4;
	private static int END_DATE_INCREMENT = START_DATE_INCREMENT + 1;
	// End Testing Values
	private static int BATCH_SIZE = 50;
	private static SimpleDateFormat WIDGET_CONTROL_FORMAT = new SimpleDateFormat("EEEE, MMMM d");
	private static String DEFAULT_TABLE_STYLE = "innerb";
	private static String EDITED_TABLE_STYLE = "innerc";
	private static NSArray<String> AVAILABLE_OPTIONS = new NSArray<String>("Date", "Time", "Name", "Mobile", "Home", "Work", "Email", "Status", "Confirmed", "Confirmation Method", "Next Transaction",
			"Last Transaction");
	private static NSArray<String> DEFAULT_OPTIONS = new NSArray<String>("Time", "Name", "Mobile", "Home", "Status");

	private Customer customer;
	private NSTimestamp startDate;
	private String formattedStartDate;
	private NSTimestamp endDate;
	private ERXDisplayGroup<NSDictionary> displayGroup;
	public NSDictionary anAppointment;
	protected String sortKey = null;
	private AppointmentTable appointmentTable;
	// Options
	public String anOption;
	private NSArray<String> selectedOptions = null;
	private NSArray<String> currentlySelectedOptions = null;

	public Appointments(WOContext context) {
		super(context);
	}

	public String widgetName() {
		return "Appointments";
	}
	
	public String printTitle() {
		return widgetName() + formattedStartDate();
	}

	public AppointmentTable appointmentTable() {
		if (appointmentTable == null) {
			appointmentTable = new AppointmentTable();
			appointmentTable.setAllOptions(availableOptions());
			appointmentTable.setSelectedOptions(selectedOptions());
		}
		return appointmentTable;
	}

	public Customer customer() {
		if (customer == null)
			customer = Customer.fetchCustomer(editingContext, Customer.CUSTOMER_ID.eq(TEST_CUSTOMER));
		return customer;
	}

	public void setStartDate(NSTimestamp startDate) {
		this.startDate = startDate;
	}

	public NSTimestamp startDate() {
		if (startDate == null) {
			int adjustedLeadTime = START_DATE_INCREMENT != 0 ? customer().emailDailyAppointmentReminderReportLeadTime() + START_DATE_INCREMENT : customer()
					.emailDailyAppointmentReminderReportLeadTime();
			startDate = ReportUtilities.dateInFuture(adjustedLeadTime);
//			startDate = new NSTimestamp(2012,12,20,0,0,0,TimeZone.getDefault());
		}
		return startDate;
	}

	public String formattedStartDate() {
		return WIDGET_CONTROL_FORMAT.format(startDate());
	}

	public NSTimestamp endDate() {
		return ReportUtilities.adjustEndDate(startDate());
	}

	public ERXDisplayGroup<NSDictionary> displayGroup() {
		if (displayGroup == null) {
			displayGroup = new ERXDisplayGroup<NSDictionary>();
			displayGroup.setNumberOfObjectsPerBatch(BATCH_SIZE);
			refetch();
		}
		return displayGroup;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	public String sortKey() {
		if (sortKey == null)
			sortKey = "date";
		return sortKey;
	}

	public NSArray<String> availableOptions() {
		return AVAILABLE_OPTIONS;
	}

	public NSArray<String> selectedOptions() {
		if (selectedOptions == null) {
			selectedOptions = DEFAULT_OPTIONS;
			setCurrentlySelectedOptions(selectedOptions);
		}
		return sortOptions(selectedOptions);
	}

	public void setSelectedOptions(NSArray<String> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public NSArray<String> currentlySelectedOptions() {
		return currentlySelectedOptions;
	}

	public void setCurrentlySelectedOptions(NSArray<String> currentlySelectedOptions) {
		this.currentlySelectedOptions = currentlySelectedOptions;
	}

	// Sort selectedOptions array to Available Options
	public NSArray<String> sortOptions(NSArray<String> options) {
		NSMutableArray<String> returnArray = new NSMutableArray<String>();
		for (String anOption : availableOptions()) {
			if (options.contains(anOption))
				returnArray.addObject(anOption);
		}
		return returnArray.immutableClone();
	}

	public String widgetColumns() {
		return appointmentTable().twoCol() ? "box two-col" : "box three-col";
	}

	public String widgetTableWrapper() {
		return appointmentTable().twoCol() ? "table_twocol_wrapper" : "table_threecol_wrapper";
	}

	public String widgetTableData() {
		return appointmentTable().twoCol() ? "table_twocol_data" : "table_threecol_data";
	}

	// When date is changed on form, clear the displayGroup and refetch the data.
	public WOActionResults formRefetch() {
		// TODO
		// need to validate if needs full reload (currently selected options / and selected options)
		// Idea - loop through options and determine if data needs to be refetched - if I remove the auto fetch.
		displayGroup = null;
		refetch();
		setCurrentlySelectedOptions(selectedOptions());
		// Need to update the DatePicker
		AjaxUpdateContainer.updateContainerWithID("AppointmentSelectedDate", context());
		return null;
	}

	public WOActionResults optionFilter() {
		appointmentTable().setSelectedOptions(selectedOptions());
		setCurrentlySelectedOptions(selectedOptions());
		return null;
	}

	// Setup the Display Group with data
	public WOActionResults refetch() {
		appointmentTable.setSelectedOptions(selectedOptions());
		NSArray<PatientAppointment> appointments = fetchPatientAppointmentsForDate(startDate());
		ERXBatchFetchUtilities.batchFetch(appointments, PatientAppointment.DATE_KEY);
		displayGroup().setObjectArray(dictionaryForData(appointments));

		EOSortOrdering dateSort = EOSortOrdering.sortOrderingWithKey("date", ERXSortOrdering.CompareAscending);
		NSArray<EOSortOrdering> sortOrderings = new NSArray(dateSort);
		setSortKey(null);
		displayGroup().setSortOrderings(sortOrderings);
		displayGroup().setCurrentBatchIndex(1);
		return null;
	}

	// Get the data from the db
	public NSArray<PatientAppointment> fetchPatientAppointmentsForDate(NSTimestamp aStartDate) {
		setStartDate(aStartDate);
		EOQualifier q1, q2, q3, qual;
		q1 = PatientAppointment.CUSTOMER_ID.eq(customer().customerId());
		q2 = PatientAppointment.DATE.between(startDate(), endDate());
		qual = ERXQ.and(q1, q2);
		ERXSortOrderings sort = PatientAppointment.DATE.ascs();
		ERXFetchSpecification<PatientAppointment> fs = new ERXFetchSpecification<PatientAppointment>(PatientAppointment.ENTITY_NAME, qual, sort);
		fs.setPrefetchingRelationshipKeyPaths(PatientAppointment.CUSTOMER_PATIENT);
		return fs.fetchObjects(editingContext);
	}

	// Convert data into a dictionary
	// Note - this is only done as the phone numbers required in the report have to be fetched each time the data is refreshed - time consuming on sorts.
	// By processing the PatientAppointment EO into a dictionary this is only done once.
	public NSArray<NSDictionary> dictionaryForData(NSArray<PatientAppointment> appointmentArray) {
		NSMutableArray<NSDictionary> returnArray = new NSMutableArray();

		for (Enumeration<PatientAppointment> apptEnum = appointmentArray.objectEnumerator(); apptEnum.hasMoreElements();) {
			PatientAppointment anAppointment = apptEnum.nextElement();
			NSMutableDictionary returnDictionary = new NSMutableDictionary();
			// Default Values
			returnDictionary.put("date", anAppointment.date());
			returnDictionary.put("name", anAppointment.customerPatient().name());
			returnDictionary.put("cellPhone", anAppointment.customerPatient().preferredMobileNumberFormatted() != null ? anAppointment.customerPatient().preferredMobileNumberFormatted() : "");
			returnDictionary.put("homePhone", anAppointment.customerPatient().preferredHomeNumberFormatted() != null ? anAppointment.customerPatient().preferredHomeNumberFormatted() : "");
			returnDictionary.put("status", anAppointment.status());
			// Optional Values
			// TODO - Build work method on customer patient
			returnDictionary.put("workPhone",
					anAppointment.customerPatient().patientPhones(PatientPhone.TYPE.eq("WORK")).lastObject() != null ? anAppointment.customerPatient().patientPhones(PatientPhone.TYPE.eq("WORK"))
							.lastObject().formattedPhoneNumber() : "");
			returnDictionary.put("email", anAppointment.customerPatient().preferredEmailAddress() != null ? anAppointment.customerPatient().preferredEmailAddress() : "");
			
			// Processing Confirmations - idea to do this only when needed instead?
//			NSDictionary confirmData = valuesForConfirmations(anAppointment.patientAppointmentConfirmations());
//			if (confirmData != null)
//				returnDictionary.addEntriesFromDictionary(confirmData);
			
			// TODO Next transaction
			// Next Trans
			NSDictionary lastTransaction = lastTransactionForAppointment(anAppointment);
			if (lastTransaction != null)
				returnDictionary.addEntriesFromDictionary(lastTransaction);
			
			// ****ORIGINAL****
			// Optional
			// Confirm Table
			// if (selectedOptions().contains("Days Since Confirm") || selectedOptions().contains("Confirmation Method")) {
			// NSDictionary confirmData = valuesForConfirmations(anAppointment.patientAppointmentConfirmations());
			// if (confirmData != null)
			// returnDictionary.addEntriesFromDictionary(confirmData);
			// }
			// *****//
			returnArray.addObject(returnDictionary.immutableClone());
		}
		return returnArray.immutableClone();
	}

	// Confirmation Data
	public NSDictionary valuesForConfirmations(NSArray<PatientAppointmentConfirmation> confirmations) {
		NSMutableDictionary returnDictionary = new NSMutableDictionary();

		if (confirmations != null) {
			// May be more than one method
			StringBuilder methodSB = new StringBuilder();
			Integer daysSinceConfirmation = 0;
			StringBuilder daysSinceSB = new StringBuilder();

			for (Enumeration<PatientAppointmentConfirmation> confirmationEnum = confirmations.objectEnumerator(); confirmationEnum.hasMoreElements();) {
				PatientAppointmentConfirmation aConfirmation = confirmationEnum.nextElement();
				if (methodSB.length() > 0)
					methodSB.append(" / ");
				methodSB.append(aConfirmation.confirmationMethod());
				// Use latest Confirmation
				if (!confirmationEnum.hasMoreElements()) {
					daysSinceConfirmation = ReportUtilities.intervalBetweenDates(aConfirmation.confirmationDate(), ERXTimestampUtilities.today());
					daysSinceSB.append(daysSinceConfirmation).append(" d ago");
				}
				returnDictionary.put("confirmed", daysSinceSB.toString());
				returnDictionary.put("confirmationMethod", methodSB.toString());
			}
		}
		return returnDictionary.immutableClone();
	}
	
	public NSDictionary lastTransactionForAppointment(PatientAppointment anAppointment) {

		NSMutableDictionary returnDictionary = new NSMutableDictionary();
		
		EOQualifier q1,q2,q3,qual;
		q1 = CustomerTransaction.CUSTOMER_ID.eq(anAppointment.customerId());
		q2 = CustomerTransaction.CUSTOMER_TRANSACTION_DETAILS.dot(CustomerTransactionDetail.PATIENT_ID).eq(anAppointment.patientId());
		q3 = CustomerTransaction.CUSTOMER_TRANSACTION_DETAILS.dot(CustomerTransactionDetail.APPOINTMENT_ID).eq(anAppointment.appointmentId());
		qual = ERXQ.and(q1,q2,q3);
		
		ERXSortOrderings sort = CustomerTransaction.TIMESTAMP.ascs();
		ERXFetchSpecification<CustomerTransaction> fs = new ERXFetchSpecification<CustomerTransaction>(CustomerTransaction.ENTITY_NAME, qual, sort);

		CustomerTransaction aTransaction = fs.fetchObjects(editingContext).lastObject();
		if (aTransaction != null)
			returnDictionary.put("lastTransaction", aTransaction.timestamp());
		
		return returnDictionary.immutableClone();
	}

	// Sort Options
	public void sortByDate() {
		setSortOrder("date");
		setSortKey("date");
	}

	public void sortByName() {
		setSortOrder("name");
		setSortKey("name");
	}

	public void sortByStatus() {
		setSortOrder("status");
		setSortKey("status");
	}

	public boolean isTime() {
		return anOption == "Time"; 
	}
	
	// Do the sort
	private void setSortOrder(String name) {

		NSArray<EOSortOrdering> oldArray = displayGroup().sortOrderings();
		EOSortOrdering oldOrdering = null;
		EOSortOrdering newOrdering = null;

		if (oldArray != null) {
			oldOrdering = oldArray.lastObject();
		}
		if (oldOrdering != null && oldOrdering.key().equals(name)) {
			newOrdering = EOSortOrdering.sortOrderingWithKey(name, oldOrdering.selector() == EOSortOrdering.CompareDescending ? EOSortOrdering.CompareAscending : EOSortOrdering.CompareDescending);
		} else {
			newOrdering = EOSortOrdering.sortOrderingWithKey(name, EOSortOrdering.CompareAscending);
		}
		NSArray<EOSortOrdering> sortOrderings = new NSArray(newOrdering);

		displayGroup().setSortOrderings(sortOrderings);
		displayGroup().qualifyDisplayGroup();
	}

	// Classes for CSS
	public String dateSortClass() {
		if (sortKey().equalsIgnoreCase("date")) {
			if (displayGroup().sortOrderings().lastObject().selector() == EOSortOrdering.CompareAscending)
				return "sortasc";
			else
				return "sortdesc";
		} else
			return null;
	}

	public String nameSortClass() {
		if (sortKey().equalsIgnoreCase("name")) {
			if (displayGroup().sortOrderings().lastObject().selector() == EOSortOrdering.CompareAscending)
				return "sortasc";
			else
				return "sortdesc";
		} else
			return null;
	}

	public String statusSortClass() {
		if (sortKey().equalsIgnoreCase("status")) {
			if (displayGroup().sortOrderings().lastObject().selector() == EOSortOrdering.CompareAscending)
				return "sortasc";
			else
				return "sortdesc";
		} else
			return null;
	}

	// End Sort Options

	// Show Edit Options
	public String showOptionsJS() {
		StringBuilder sb = new StringBuilder();
		sb.append("function show_appointment_options(){");
		sb.append("var menu = document.getElementById('appointments_option_menu');");
		sb.append("if(menu.style.display == 'block'){");
		sb.append("menu.style.display = 'none';");
		sb.append("} else {");
		sb.append("menu.style.display = 'block';}}");
		return sb.toString();
	}

	// Data Columns
	public String showColumnsJS() {
		StringBuilder sb = new StringBuilder();
		sb.append("function show_appointment_columns(){");
		sb.append("var menu = document.getElementById('column_menu');");
		sb.append("if(menu.style.display == 'block'){");
		sb.append("menu.style.display = 'none';");
		sb.append("} else {");
		sb.append("menu.style.display = 'block';}}");
		return sb.toString();
	}

	// Export to Excel.
	public WOActionResults exportToExcel() {
		AppointmentsExcel excelPage =  pageWithName(AppointmentsExcel.class);
    	excelPage.setSelectedOptions(selectedOptions());
    	excelPage.setAppointmentData(displayGroup().displayedObjects());
    	excelPage.setFileName(formattedStartDate());
    	return excelPage;	
    }

	// Building Column Style Loop
	public String columnStyle() {
		return (String) appointmentTable().columnWidths().valueForKey(anOption);
	}

	// TODO - COLUMN CLASS
	public String columnClass() {
		if (anOption.equalsIgnoreCase("time") && dateSortClass() != null)
			return dateSortClass();
		else if (anOption.equalsIgnoreCase("name") && nameSortClass() != null)
			return nameSortClass();
		else if (anOption.equalsIgnoreCase("status") && statusSortClass() != null)
			return statusSortClass();
		else
			return null;
	}
	
	public boolean isDate() {
		return Boolean.TRUE.equals(anOption.equalsIgnoreCase("Date")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Time")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Next Transaction"))
				|| Boolean.TRUE.equals(anOption.equalsIgnoreCase("Last Transaction"));
	}

	public String dateFormat() {
		if (Boolean.TRUE.equals(anOption.equalsIgnoreCase("Date")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Last Transaction")))
			return "MM/dd";
		else if (Boolean.TRUE.equals(anOption.equalsIgnoreCase("Time")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Next Transaction")))
			return "h:mm a";
		else
			return "";
	}
	
	public Object valueToShow() {
		return anAppointment.valueForKey(convertOptionToDictionary(anOption));
	}

	public String convertOptionToDictionary(String anOption) {
		if (anOption.equalsIgnoreCase("date"))
			return "date";
		else if (anOption.equalsIgnoreCase("time"))
			return "date";
		else if (anOption.equalsIgnoreCase("name"))
			return "name";
		else if (anOption.equalsIgnoreCase("mobile"))
			return "cellPhone";
		else if (anOption.equalsIgnoreCase("home"))
			return "homePhone";
		else if (anOption.equalsIgnoreCase("work"))
			return "workPhone";
		else if (anOption.equalsIgnoreCase("email"))
			return "email";
		else if (anOption.equalsIgnoreCase("status"))
			return "status";
		else if (anOption.equalsIgnoreCase("confirmed"))
			return "confirmed";
		else if (anOption.equalsIgnoreCase("next transaction"))
			return "nextTransaction";
		else if (anOption.equalsIgnoreCase("last transaction"))
			return "lastTransaction";
		else if (anOption.equalsIgnoreCase("confirmation method"))
			return "confirmationMethod";		
		else
			return "";
	}

	/**
	 * Appointment Table width calculations
	 * 
	 * @author tcmartin
	 * 
	 */
	static public class AppointmentTable {
		private static Integer DEFAULT_WIDTH = 654;
		private static Integer MAX_WIDTH = 994;
		private static Integer PADDING = 16;
		//private static Integer DATE_WIDTH = 99;
		//private static Integer NAME_WIDTH = 148;
		private static Integer DATE_WIDTH = 40;
		private static Integer TIME_WIDTH = 60;
		private static Integer NAME_WIDTH = 140;
		private static Integer PHONE_WIDTH = 109;
		private static Integer EMAIL_WIDTH = 148;
		private static Integer STATUS_WIDTH = 100;
		//private static Integer STATUS_WIDTH = 156;
		private static Integer CONFIRM_DAYS_WIDTH = 66;
		private static Integer NEXT_TRANS_WIDTH = 75;
		private static Integer LAST_TRANS_WIDTH = 75;
		private static Integer CONFIRM_STATUS_WIDTH = 109;
		private static Integer BREAKPOINT = 7;
		private NSArray<String> allOptions;
		private NSArray<String> selectedOptions;

		public AppointmentTable() {
		}

		public NSArray<String> selectedOptions() {
			return selectedOptions;
		}

		public void setSelectedOptions(NSArray<String> selectedOptions) {
			this.selectedOptions = selectedOptions;
		}

		public NSArray<String> allOptions() {
			return allOptions;
		}

		public void setAllOptions(NSArray<String> allOptions) {
			this.allOptions = allOptions;
		}

		public Integer columns() {
			return selectedOptions().count();
		}
		
		public boolean twoCol() {
			return selectedColumnWidths() <= DEFAULT_WIDTH;
		}
		
		public String tableWidth() {
			Integer width = 0;
			for (Enumeration selectionEnum = selectedOptions().objectEnumerator(); selectionEnum.hasMoreElements();) {
				String anOption = (String) selectionEnum.nextElement();
				Integer adjustedWidth = (Integer) columnWidthValues().valueForKey(anOption) + PADDING;
				width += adjustedWidth;
			}

			// Adjust width to be at least the default width
			Integer adjustedWidth = twoCol() ? width <= DEFAULT_WIDTH ? DEFAULT_WIDTH : width : width < MAX_WIDTH ? MAX_WIDTH : width;
			return "width:" + adjustedWidth + "px;";
		}
		
		public Integer selectedColumnWidths() {
			
			Integer width = 0;
			
			for (Enumeration selectionEnum = selectedOptions().objectEnumerator(); selectionEnum.hasMoreElements();) {
				String anOption = (String) selectionEnum.nextElement();
				Integer adjustedWidth = (Integer) columnWidthValues().valueForKey(anOption) + PADDING;
				width += adjustedWidth;
			}
			
			return width;
		}

		public NSDictionary<String, Integer> allColumnWidthValues() {

			NSMutableDictionary<String, Integer> returnValues = new NSMutableDictionary<String, Integer>();
			
			for (Enumeration selectionEnum = allOptions().objectEnumerator(); selectionEnum.hasMoreElements();) {
				String anOption = (String) selectionEnum.nextElement();
				Integer aWidth = widthForOption(anOption);
				returnValues.put(anOption, aWidth);
			}
			return returnValues.immutableClone();
		}

		public NSDictionary<String, Integer> columnWidthValues() {
			
			NSMutableDictionary<String, Integer> returnValues = new NSMutableDictionary<String, Integer>();
			
			for (Enumeration selectionEnum = selectedOptions().objectEnumerator(); selectionEnum.hasMoreElements();) {
				String anOption = (String) selectionEnum.nextElement();
				Integer aWidth = widthForOption(anOption);
				returnValues.put(anOption, aWidth);
			}
			return returnValues.immutableClone();
		}

		public NSDictionary<String, String> columnWidths() {
			
			NSMutableDictionary<String, String> returnValues = new NSMutableDictionary<String, String>();
			
			for (String aKey : allColumnWidthValues().allKeys()) {
				Integer aValue = (Integer) allColumnWidthValues().valueForKey(aKey);
				String valueString = "width:" + aValue + "px;";
				returnValues.put(aKey, valueString);
			}
			return returnValues.immutableClone();
		}

		public Integer widthForOption(String anOption) {
			
			if (anOption.equalsIgnoreCase("Date"))
				return DATE_WIDTH;
			else if (anOption.equalsIgnoreCase("Time"))
				return TIME_WIDTH;
			else if (anOption.equalsIgnoreCase("Name"))
				return NAME_WIDTH;
			else if (anOption.equalsIgnoreCase("Mobile"))
				return PHONE_WIDTH;
			else if (anOption.equalsIgnoreCase("Home"))
				return PHONE_WIDTH;
			else if (anOption.equalsIgnoreCase("Work"))
				return PHONE_WIDTH;
			else if (anOption.equalsIgnoreCase("Email"))
				return EMAIL_WIDTH;
			else if (anOption.equalsIgnoreCase("Status"))
				return STATUS_WIDTH;
			else if (anOption.equalsIgnoreCase("Confirmed"))
				return CONFIRM_DAYS_WIDTH;
			else if (anOption.equalsIgnoreCase("Confirmation Method"))
				return CONFIRM_STATUS_WIDTH;
			else if (anOption.equalsIgnoreCase("Next Transaction"))
				return NEXT_TRANS_WIDTH;
			else if (anOption.equalsIgnoreCase("Last Transaction"))
				return LAST_TRANS_WIDTH;
			else
				return 0;
		}
	}

	static public class Option {
		private String shortName;
		private String fullName;
		private String header;
		private Integer minWidth;
		private Integer maxWidth;
		private String dateFormat;

		public Option() {
		}

		public String shortName() {
			return shortName;
		}

		public void setShortName(String shortName) {
			this.shortName = shortName;
		}

		public String fullName() {
			return fullName;
		}

		public void setFullName(String fullName) {
			this.fullName = fullName;
		}

		public String header() {
			return header;
		}

		public void setHeader(String header) {
			this.header = header;
		}

		public Integer minWidth() {
			return minWidth;
		}

		public void setMinWidth(Integer width) {
			this.minWidth = width;
		}
		
		public void setMaxWidth(Integer width) {
			this.maxWidth = width;
		}
		
		public Integer maxWidth() {
			return maxWidth;
		}

		public String columnStyle() {
			return "width:" + minWidth() + "px;";
		}
		
		public void setDateFormat(String dateFormat) {
			this.dateFormat = dateFormat;
		}
		
		public String dateFormat() {
			return dateFormat;
		}	
	}
}