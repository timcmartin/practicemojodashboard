package com.practicemojo.com.woapp.dashboard.widgets;

import org.apache.log4j.Logger;

import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class SpinnerTester extends SHWOComponent {
	private static final Logger log = Logger.getLogger(SpinnerTester.class);
    private NSTimestamp startDate;
	
	public SpinnerTester(WOContext context) {
        super(context);
    }
    
    public NSTimestamp startDate() {
    	return new NSTimestamp();
    }
    
    public void setStartDate(NSTimestamp startDate) {
    	this.startDate = startDate;
    }
    
	// start the ajax spinner
	public String startSpinner() {
		log.info("STARTING");
		return "$('oneColSaveSpinner').show('slow');";
	}
	
	// stop the ajax spinner
	public String stopSpinner() {
		log.info("STOPPING");
		return "function(){$('oneColSaveSpinner').hide();}";
	}
	
	public WOActionResults testAction() {
		log.info("TEST ACTION CALLED");
		long t0, t1;
		t0 = System.currentTimeMillis();
		do{
			t1=System.currentTimeMillis();
		} while (t1-t0 < 3000);
		log.info("Test ACtion Finished");
		return null;
	}
}