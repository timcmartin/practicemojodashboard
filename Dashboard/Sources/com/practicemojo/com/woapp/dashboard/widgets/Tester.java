package com.practicemojo.com.woapp.dashboard.widgets;

import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.practicemojo.businesslogic.customer.Customer;
import com.practicemojo.businesslogic.reports.ReportUtilities;
import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXArrayUtilities;

public class Tester extends SHWOComponent {
	private static final Logger log = Logger.getLogger(Tester.class);
	public EOEditingContext editingContext = ERXEC.newEditingContext();
	private static boolean USE_TEST_CUSTOMER = true;
	private static int TEST_CUSTOMER = 2530;
	private static SimpleDateFormat WIDGET_CONTROL_FORMAT = new SimpleDateFormat("EEEE, MMMM d");
	private Customer customer;
	private NSTimestamp startDate;
	private NSTimestamp endDate;
	public NSDictionary confirmations;
	public Boolean completedValue;
	private NSArray<NSMutableDictionary> finalValues;
	public NSArray<NSDictionary> finalValueList;

	public Tester(WOContext context) {
		super(context);
	}

	public String widgetName() {
		return "Tester";
	}
	
	public void setStartDate(NSTimestamp startDate) {
		this.startDate = startDate;
	}
	
	public NSTimestamp startDate() {
		return new NSTimestamp();
	}
	
	public NSTimestamp endDate() {
		return ReportUtilities.adjustEndDate(startDate());
	}

	public NSArray<NSMutableDictionary> finalValues() {
		if (finalValues == null) {
			NSMutableArray<NSMutableDictionary> returnValues = new NSMutableArray<NSMutableDictionary>(); 
			returnValues.addObject(fred());
			returnValues.addObject(steve());
			finalValues = returnValues;
		}

		return finalValues;
	}
	
	public NSDictionary<Boolean, NSArray<NSMutableDictionary>> groupedFinalValues() {
		log.info(ERXArrayUtilities.arrayGroupedByKeyPath(finalValues(), "completed"));
		return ERXArrayUtilities.arrayGroupedByKeyPath(finalValues(), "completed");
	}
	
	public boolean isSelected() {
		//return anAppointment.status().equalsIgnoreCase("Confirmed");
		return false;
	}

	public WOActionResults options() {
		return null;
	}
	
	public ConfirmationTable confirmationTable() {
		return new ConfirmationTable();
	}
	
	public WOActionResults formRefetch() {
		log.info("*****FORM REFETCH CALLED*****");
		// TODO
		// need to validate if needs full reload (currently selected options / and selected options)
		// Idea - loop through options and determine if data needs to be refetched - if I remove the auto fetch.
		//displayGroup = null;
		//refetch();
		//setCurrentlySelectedOptions(selectedOptions());
		// Need to update the DatePicker
		AjaxUpdateContainer.updateContainerWithID("SelectedDate", context());
		return null;
	}
	
	public WOActionResults checkBoxRefetch() {
		log.info("CHECK BOX CALLED");
		log.info(confirmations.valueForKey("name") + " : " + confirmations.valueForKey("completed"));
		AjaxUpdateContainer.updateContainerWithID("ConfirmationData", context());
		return null;
	}
	
	static public class ConfirmationTable {
		private static Integer DEFAULT_WIDTH = 314;
		private static Integer MAX_WIDTH = 314;
		private static Integer PADDING = 16;
		private static Integer DATE_WIDTH = 40;
		private static Integer TIME_WIDTH = 60;
		private static Integer NAME_WIDTH = 76;
		private static Integer SOURCE_WIDTH = 40;
		private static Integer COMPLETE_WIDTH = 18;
		
		public ConfirmationTable() {
		}
		
		public String tableWidth() {
			return "width:" + DEFAULT_WIDTH + "px;";
		}
		
		public String dateWidth() {
			return "width:" + DATE_WIDTH + "px;";			
		}

		public String timeWidth() {
			return "width:" + TIME_WIDTH + "px;";			
		}
		
		public String nameWidth() {
			return "width:" + NAME_WIDTH + "px;";			
		}

		public String sourceWidth() {
			return "width:" + SOURCE_WIDTH + "px;";			
		}
		
		public String completeWidth() {
			return "width:" + COMPLETE_WIDTH + "px;";			
		}
	}

	public NSMutableDictionary fred() {
		NSMutableDictionary aValue = new NSMutableDictionary();
		aValue.put("date", new NSTimestamp());
		aValue.put("name", "Fred Sanford");
		aValue.put("method", "EMAIL");
		aValue.put("completed", true);
		return aValue;
	}

	public NSMutableDictionary steve() {
		NSMutableDictionary aValue = new NSMutableDictionary();
		aValue.put("date", new NSTimestamp());
		aValue.put("name", "Steve Yzerman");
		aValue.put("method", "SMS");
		aValue.put("completed", false);
		return aValue;
	}
}