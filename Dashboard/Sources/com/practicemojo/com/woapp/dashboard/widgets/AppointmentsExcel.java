package com.practicemojo.com.woapp.dashboard.widgets;

import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class AppointmentsExcel extends SHWOComponent {
	private String fileName;
	private NSArray<NSDictionary> appointmentData;
	public NSDictionary anAppointment;
	private NSArray<String> selectedOptions;
	public String anOption;
	
	public AppointmentsExcel(WOContext context) {
        super(context);
    }
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String fileName() {
		return "Appointments " + fileName;
	}
	
	public void setAppointmentData(NSArray<NSDictionary> appointmentData) {
		this.appointmentData = appointmentData;
	}
	
	public NSArray<NSDictionary> appointmentData() {
		return appointmentData;
	}
	
	public void setSelectedOptions(NSArray<String> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	
	public NSArray<String> selectedOptions() {
		return selectedOptions;
	}

	public boolean isDate() {
		return Boolean.TRUE.equals(anOption.equalsIgnoreCase("Date")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Time")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Next Transaction"))
				|| Boolean.TRUE.equals(anOption.equalsIgnoreCase("Last Transaction"));
	}

	public String dateFormat() {
		if (Boolean.TRUE.equals(anOption.equalsIgnoreCase("Date")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Last Transaction")))
			return "MM/dd";
		else if (Boolean.TRUE.equals(anOption.equalsIgnoreCase("Time")) || Boolean.TRUE.equals(anOption.equalsIgnoreCase("Next Transaction")))
			return "h:mm a";
		else
			return "";
	}
	
	public Object valueToShow() {
		return anAppointment.valueForKey(convertOptionToDictionary(anOption));
	}
	
	public String convertOptionToDictionary(String anOption) {
		if (anOption.equalsIgnoreCase("date"))
			return "date";
		else if (anOption.equalsIgnoreCase("time"))
			return "date";
		else if (anOption.equalsIgnoreCase("name"))
			return "name";
		else if (anOption.equalsIgnoreCase("mobile"))
			return "cellPhone";
		else if (anOption.equalsIgnoreCase("home"))
			return "homePhone";
		else if (anOption.equalsIgnoreCase("work"))
			return "workPhone";
		else if (anOption.equalsIgnoreCase("email"))
			return "email";
		else if (anOption.equalsIgnoreCase("status"))
			return "status";
		else if (anOption.equalsIgnoreCase("confirmed"))
			return "confirmed";
		else if (anOption.equalsIgnoreCase("next transaction"))
			return "nextTransaction";
		else if (anOption.equalsIgnoreCase("last transaction"))
			return "lastTransaction";
		else if (anOption.equalsIgnoreCase("confirmation method"))
			return "confirmationMethod";		
		else
			return "";
	}
}