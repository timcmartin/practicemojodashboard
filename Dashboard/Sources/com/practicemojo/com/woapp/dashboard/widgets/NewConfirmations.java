package com.practicemojo.com.woapp.dashboard.widgets;

import java.text.SimpleDateFormat;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.practicemojo.businesslogic.PracticeMojoBusinessLogic;
import com.practicemojo.businesslogic.customer.Customer;
import com.practicemojo.businesslogic.customer.PatientAppointment;
import com.practicemojo.businesslogic.customer.PatientAppointmentConfirmation;
import com.practicemojo.businesslogic.reports.ReportUtilities;
import com.smarthealth.webobjects.SHWOComponent;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXBatchFetchUtilities;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

public class NewConfirmations extends SHWOComponent {
	private static final Logger log = Logger.getLogger(NewConfirmations.class);
	private static boolean USE_TEST_CUSTOMER = PracticeMojoBusinessLogic.useDashboardTestCustomer();
	private static int TEST_CUSTOMER = 2530;
	private static SimpleDateFormat WIDGET_CONTROL_FORMAT = new SimpleDateFormat("EEEE, MMMM d");
	private static int BATCH_SIZE = 50;
	private Customer customer;
	private NSTimestamp startDate;
	private NSTimestamp endDate;
	private ERXDisplayGroup<NSMutableDictionary> displayGroup;
	public NSMutableDictionary displayDictionary;
	public EOEditingContext editingContext = ERXEC.newEditingContext();
	
	public NewConfirmations(WOContext context) {
        super(context);
    }
    
	public String widgetName() {
		return "New Confirmations";
	}

	public Customer customer() {
		if (USE_TEST_CUSTOMER) {
			if (customer == null)
				customer = Customer.fetchCustomer(editingContext, Customer.CUSTOMER_ID.eq(TEST_CUSTOMER));
		} else {
			if (customer == null) 
				//customer = ((Session) session()).customer();
				customer = null;
		}
		return customer;
	}
	
	public void setStartDate(NSTimestamp startDate) {
		this.startDate = startDate;
	}
	
	public NSTimestamp startDate() {
		if (startDate == null) {
			int adjustedLeadTime = customer().emailDailyAppointmentReminderReportLeadTime() != null ? customer().emailDailyAppointmentReminderReportLeadTime() - 1 : 1;
			startDate = ReportUtilities.dateInFuture(adjustedLeadTime);
		}
		return startDate;
	}
	
	public NSTimestamp endDate() {
		return ReportUtilities.adjustEndDate(startDate());
	}

	public ERXDisplayGroup<NSMutableDictionary> displayGroup() {
		if (displayGroup == null) {
			displayGroup = new ERXDisplayGroup<NSMutableDictionary>();
			displayGroup.setNumberOfObjectsPerBatch(BATCH_SIZE);
			refetch();			
		}
		return displayGroup;
	}
	
	// Setup the Display Group with data
	public WOActionResults refetch() {
		NSArray<PatientAppointment> appointments = fetchPatientAppointmentsForDate(startDate());
		ERXBatchFetchUtilities.batchFetch(appointments, PatientAppointment.DATE_KEY);
		displayGroup().setObjectArray(dictionaryForData(appointments));
		resortDisplayGroup();
		displayGroup().setCurrentBatchIndex(1);
		return null;
	}

	// Sort the data;
	public void resortDisplayGroup() {
		EOSortOrdering dateSort = EOSortOrdering.sortOrderingWithKey("date", ERXSortOrdering.CompareAscending);
		EOSortOrdering completeSort = EOSortOrdering.sortOrderingWithKey("completed", ERXSortOrdering.CompareAscending);
		NSArray<EOSortOrdering> sortOrderings = new NSArray(completeSort, dateSort);
		displayGroup().setSortOrderings(sortOrderings);
		displayGroup().updateDisplayedObjects();
	}
	
	// Get the data from the db
	public NSArray<PatientAppointment> fetchPatientAppointmentsForDate(NSTimestamp aStartDate) {
		setStartDate(aStartDate);
		ERXSortOrderings sort = PatientAppointment.DATE.ascs();
		ERXFetchSpecification<PatientAppointment> fs = new ERXFetchSpecification<PatientAppointment>(PatientAppointment.ENTITY_NAME, appointmentFetchQualifier(), sort);
		return fs.fetchObjects(editingContext);
	}

	// This data is often empty
	// This qualifier method is so that one can test with some actual data
	public EOQualifier appointmentFetchQualifier() {
		EOQualifier q1, q2, q3, qual;
		q1 = PatientAppointment.CUSTOMER_ID.eq(customer().customerId());
		q2 = PatientAppointment.DATE.between(startDate(), endDate());
		q3 = PatientAppointment.STATUS.eq("Scheduled"); // Only worrying about non-confirms in PMS
		if (USE_TEST_CUSTOMER) {
			qual = ERXQ.and(q1,q2);
		} else {
			qual = ERXQ.and(q1,q2,q3);
		}
		return qual;
	}
	
	// Format Data in Dictionary Needed
	public NSArray<NSMutableDictionary> dictionaryForData(NSArray<PatientAppointment> appointmentArray) {
		NSMutableArray<NSMutableDictionary> returnArray = new NSMutableArray<NSMutableDictionary>();

		for (Enumeration<PatientAppointment> apptEnum = appointmentArray.objectEnumerator(); apptEnum.hasMoreElements();) {
			PatientAppointment anAppointment = apptEnum.nextElement();

			 if (anAppointment.isConfirmedPAC()) {

				String name = anAppointment.customerPatient().name();
				NSTimestamp date = anAppointment.date();
				StringBuilder methodSB = new StringBuilder();
				
				for (Enumeration<PatientAppointmentConfirmation> pacEnum = anAppointment.patientAppointmentConfirmations().objectEnumerator(); pacEnum.hasMoreElements();) {
					PatientAppointmentConfirmation aConfirmation = pacEnum.nextElement();
				
					if (methodSB.length() > 0)
						methodSB.append(" / ");

					methodSB.append(aConfirmation.confirmationMethod());
				}
					NSMutableDictionary aValue = new NSMutableDictionary();
					aValue.put("date", date);
					aValue.put("name", name);
					aValue.put("method", methodSB.toString());
					aValue.put("completed", false);
					returnArray.addObject(aValue);
				}
			 }

		return returnArray.immutableClone();
	}
	
	// Reorder Data When User Checks Box
	public WOActionResults checkbox() {
		log.info("Checked Box");
		resortDisplayGroup();
		AjaxUpdateContainer.updateContainerWithID("ConfirmationData", context());
		return null;
	}

	// Show Edit Options
	public String showOptionsJS() {
		StringBuilder sb = new StringBuilder();
		sb.append("function show_options(){");
		sb.append("var menu = document.getElementById('new_conf_option_menu');");
		sb.append("if(menu.style.display == 'block'){");
		sb.append("menu.style.display = 'none';");
		sb.append("} else {");
		sb.append("menu.style.display = 'block';}}");
		return sb.toString();
	}

	// Menu Actions - Export to Excel.
	public WOActionResults exportToExcel() {
//		AppointmentsExcel excelPage =  pageWithName(AppointmentsExcel.class);
//    	excelPage.setSelectedOptions(selectedOptions());
//    	excelPage.setAppointmentData(displayGroup().displayedObjects());
//    	excelPage.setFileName(formattedStartDate());
//    	return excelPage;	
		return null;
	}

	public ConfirmationTable confirmationTable() {
		return new ConfirmationTable();
	}
	
	public WOActionResults formRefetch() {
		displayGroup = null;
		refetch();
		// Need to update the DatePicker
		AjaxUpdateContainer.updateContainerWithID("ConfirmationSelectedDate", context());
		return null;
	}
	
	static public class ConfirmationTable {
		private static Integer DEFAULT_WIDTH = 314;
		private static Integer MAX_WIDTH = 314;
		private static Integer PADDING = 16;
		private static Integer DATE_WIDTH = 40;
		private static Integer TIME_WIDTH = 60;
		private static Integer NAME_WIDTH = 76;
		private static Integer SOURCE_WIDTH = 40;
		private static Integer COMPLETE_WIDTH = 18;
		
		public ConfirmationTable() {
		}
		
		public String tableWidth() {
			return "width:" + DEFAULT_WIDTH + "px;";
		}
		
		public String dateWidth() {
			return "width:" + DATE_WIDTH + "px;";			
		}

		public String timeWidth() {
			return "width:" + TIME_WIDTH + "px;";			
		}
		
		public String nameWidth() {
			return "width:" + NAME_WIDTH + "px;";			
		}

		public String sourceWidth() {
			return "width:" + SOURCE_WIDTH + "px;";			
		}
		
		public String completeWidth() {
			return "width:" + COMPLETE_WIDTH + "px;";			
		}
	}
	
	// start the ajax spinner
	public String startSpinner() {
		return "$('saveSpinner').show('slow');";
	}
	
	// stop the ajax spinner
	public String stopSpinner() {
		return "function(){$('saveSpinner').hide();}";
	}

}